import javax.swing.JFrame;
import javax.swing.Timer;

public class Snake extends JFrame {
	private static final long serialVersionUID = 1L;
	
	/* Some properties. */
	private final int BOARD_WIDTH = 20 * 30; // Tilesize * number of columns
	private final int BOARD_HEIGHT = 20 * 30;
	private final int DELAY = 100;

	public Snake() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(BOARD_WIDTH, BOARD_HEIGHT);
		setResizable(false);
		setLocation(50, 50);

		Board b = new Board(this, BOARD_WIDTH, BOARD_HEIGHT);
		addKeyListener(b);
		add(b);
		setVisible(true);

		Timer t = new Timer(DELAY, b);
		t.start();
	}
}